pragma solidity ^0.4.24;

import 'openzeppelin-solidity/contracts/token/ERC20/StandardToken.sol';

contract IBCToken is StandardToken {

    string public constant name = "IBC Token";
    string public constant symbol = "IBC";
    uint8 public constant decimals = 18;

    uint256 public constant INITIAL_SUPPLY = 600000000;

    constructor() public {
        totalSupply_ = INITIAL_SUPPLY * 10 ** uint256(decimals);
    }

}
