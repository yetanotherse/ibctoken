pragma solidity ^0.4.24;

import './IBCToken.sol';
import 'openzeppelin-solidity/contracts/crowdsale/Crowdsale.sol';

contract IBCTokenSale is Crowdsale {

    uint256 public constant startTime = 1533517200; // 6th August (Monday) 9 AM ET in seconds from epoch
    uint256 requiredBlockHeightForStart;

    constructor(uint256 _rate, address _wallet, IBCToken _token) public Crowdsale(_rate, _wallet, _token) {
        // calculate required block height assuming 15 second block time
        requiredBlockHeightForStart = block.number + ((startTime - now) / 15);
    }

    // modifier to ensure sale can't begin before specified date/time
    modifier onlyWhileOpen {
        /* As an altrenative, we could simply compare the required date/time converted to
        epoch with now (block.timestamp) to determine if sale should begin */
        require(block.number >= requiredBlockHeightForStart);
        _;
    }

    function _preValidatePurchase(address _beneficiary, uint256 _weiAmount) internal onlyWhileOpen {
        super._preValidatePurchase(_beneficiary, _weiAmount);
    }

}
