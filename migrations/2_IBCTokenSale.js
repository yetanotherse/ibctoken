var IBCTokenSale = artifacts.require("./IBCTokenSale.sol");
var IBCToken = artifacts.require("./IBCToken.sol");

module.exports = function(deployer, network, accounts) {

    const rate = new web3.BigNumber(1000);
    const wallet = '0x320F257688859768a45D01a37bd7d6152E56950c';

    return deployer
        .then(() => {
            return deployer.deploy(IBCToken);
        })
        .then(() => {
            return deployer.deploy(
                IBCTokenSale,
                rate,
                wallet,
                IBCToken.address
            );
        });
};
