var IBCTokenSale = artifacts.require("IBCTokenSale");
var IBCToken = artifacts.require("IBCToken");

contract('IBCTokenSale', function(accounts) {
    it('should deploy the token and store the address', function(done){
        IBCTokenSale.deployed().then(async function(instance) {
            const token = await instance.token.call();
            assert(token, 'Token address couldn\'t be stored');
            done();
       });
    });
});
