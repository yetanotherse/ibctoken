var HDWalletProvider = require("truffle-hdwallet-provider");
var infura_apikey = "6lx7mvNQfkL9iQWGXGAF ";
var mnemonic = "one two three four five six seven eight nine ten eleven twelve";

module.exports = {
    networks: {
        ropsten: {
            provider: function() {
                return new HDWalletProvider(mnemonic, "https://ropsten.infura.io/" + infura_apikey);
            },
            network_id: 3,
            gas: 4500000
        },
        development: {
            host: "127.0.0.1",
            port: 7545,
            network_id: "*"
        }
    },
    solc: {
        optimizer: {
            enabled: true,
            runs: 200
        }
    }
};
